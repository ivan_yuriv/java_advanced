package service;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Project;
import org.junit.jupiter.api.*;
import service.impl.ProjectServiceImpl;

import java.sql.SQLException;
import java.util.List;

public class ProjectServiceImplTest {
    private static ProjectServiceImpl projectService;

    @BeforeAll
    static void init() throws SQLException, ClassNotFoundException {
        projectService = new ProjectServiceImpl();
    }

    @Test
    @DisplayName("Read all projects")
    public void readAllTest() throws SQLException {
        List<Project> projectList = projectService.readAll();
        Assertions.assertTrue(projectList.size() > 0);

    }

    @Test
    @DisplayName("Read project by id")
    public void readByIdTest() throws NotFoundException, SQLException {
        int testId = 1;
        Project expected = new Project(testId, "IOS App", 10000);
        Project actual = projectService.read(testId);
        Assertions.assertEquals(expected, actual);

    }

    @Test
    @DisplayName("Create new project")
    public void createTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 11;
        Project projectToInsert = new Project(id, "Test OS", 100);
        projectService.create(projectToInsert);
        Project actual = projectService.read(id);
        Assertions.assertEquals(projectToInsert, actual);
        projectService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> projectService.read(id));

    }

    @Test
    @DisplayName("Update project")
    public void updateTest() throws NotFoundException, SQLException, AlreadyExistException {
        int id = 3;
        Project previous = projectService.read(id);
        int updatedId = 10;
        Project current = new Project(updatedId, "Test OS", 100);
        projectService.update(id, current);
        Assertions.assertEquals(current, projectService.read(updatedId));
        projectService.update(updatedId, previous);
        Assertions.assertEquals(previous, projectService.read(id));

    }

}

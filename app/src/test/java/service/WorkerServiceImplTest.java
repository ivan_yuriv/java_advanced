package service;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Worker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.impl.WorkerServiceImpl;

import java.sql.SQLException;
import java.util.List;

public class WorkerServiceImplTest {
    private static WorkerServiceImpl workerService;

    @BeforeAll
    static void init() {
        workerService = new WorkerServiceImpl();
    }

    @Test
    @DisplayName("Read all workers")
    public void readAllTest() throws SQLException {
        List<Worker> workerList = workerService.readAll();
        Assertions.assertTrue(workerList.size() > 0);
    }

    @Test
    @DisplayName("Read worker by id")
    public void readTest() throws NotFoundException, SQLException {
        int testId = 0;
        Worker expected = new Worker(testId, "Test_name", "Test_title", 100, 1);
        Worker actual = workerService.read(testId);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Create new worker")
    public void createTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 5;
        Worker workerToInsert = new Worker(id, "Test_name2", "Test_title", 200, 1);
        workerService.create(workerToInsert);
        Worker actual = workerService.read(id);
        Assertions.assertEquals(workerToInsert, actual);
        workerService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> workerService.read(id));
    }

    @Test
    @DisplayName("Update worker")
    public void updateTest() throws NotFoundException, SQLException, AlreadyExistException {
        int id = 0;
        Worker previous = workerService.read(id);
        int updatedId = 10;
        Worker current = new Worker(updatedId, "test_name3", "Test_title", 400, 2);
        workerService.update(id, current);
        Assertions.assertEquals(current, workerService.read(updatedId));
        workerService.update(updatedId, previous);
        Assertions.assertEquals(previous, workerService.read(id));
    }

}

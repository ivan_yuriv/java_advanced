package service;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.TeamLead;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.impl.TeamLeadServiceImpl;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class TeamLeadServiceImplTest {
    private static TeamLeadServiceImpl teamLeadService;

    @BeforeAll
    static void init() {
        teamLeadService = new TeamLeadServiceImpl();
    }

    @Test
    @DisplayName("Read all Team Leads")
    public void readAllTest() throws SQLException {
        List<TeamLead> teamLeadList = teamLeadService.readAll();
        Assertions.assertTrue(teamLeadList.size() > 0);
    }

    @Test
    @DisplayName("Read Team Lead by id")
    public void readTest() throws NotFoundException, SQLException {
        int testId = 1;
        TeamLead expected = new TeamLead(testId, "Yuriv Ivan", new Timestamp(System.currentTimeMillis()), 1500);
        TeamLead actual = teamLeadService.read(testId);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Create new Team Lead")
    public void createTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 5;
        TeamLead teamLeadToInsert = new TeamLead(id, "Test_name", new Timestamp(System.currentTimeMillis()), 100);
        teamLeadService.create(teamLeadToInsert);
        TeamLead actual = teamLeadService.read(id);
        Assertions.assertEquals(teamLeadToInsert, actual);
        teamLeadService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> teamLeadService.read(id));
    }
}

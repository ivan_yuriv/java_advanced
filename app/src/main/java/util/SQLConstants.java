package util;

public class SQLConstants {
    public static final String GET_ALL_PROJECTS = "SELECT * FROM app.project";
    public static final String GET_PROJECT_BY_ID = "SELECT * FROM app.project WHERE id = ?";
    public static final String INSERT_PROJECT = "INSERT INTO app.project (id, name, budget) VALUES (?, ?, ?)";
    public static final String UPDATE_PROJECT = "UPDATE app.project SET id = ?, name = ?, budget = ? WHERE (id = ?)";
    public static final String DELETE_PROJECT_BY_ID = "DELETE FROM app.project WHERE id = ?";

    public static final String GET_ALL_WORKERS = "SELECT * FROM app.worker";
    public static final String GET_WORKER_BY_ID = "SELECT * FROM app.worker WHERE id = ?";
    public static final String INSERT_WORKER = "INSERT INTO app.worker (id, full_name, title, salary) VALUES (?, ?, ?, ?)";
    public static final String UPDATE_WORKER = "UPDATE app.worker SET id = ?, full_name = ?, title = ?, salary = ? WHERE (id = ?)";
    public static final String DELETE_WORKER_BY_ID = "DELETE FROM app.worker WHERE id = ?";

    public static final String GET_ALL_TEAM_LEADS = "SELECT * FROM app.team_lead";
    public static final String GET_TEAM_LEAD_BY_ID = "SELECT * FROM app.team_lead WHERE id = ?";
    public static final String INSERT_TEAM_LEAD = "INSERT INTO app.team_lead (id, full_name, date_of_birth, salary) VALUES (?, ?, ?, ?)";
    public static final String DELETE_TEAM_LEAD_BY_ID = "DELETE FROM app.team_lead WHERE id = ?";


}

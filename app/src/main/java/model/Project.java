package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {
    private int id;
    private String name;
    private int budget;

    public Project(String name, int budget) {
        this.name = name;
        this.budget = budget;
    }
}

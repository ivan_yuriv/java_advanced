create database if not exists app;
use app;

create table if not exists project(
id int not null auto_increment primary key,
name varchar(50) not null,
budget int not null
);

create table if not exists team_lead(
id int not null auto_increment primary key,
full_name varchar(50) not null,
date_of_birth timestamp,
salary double not null,
foreign key(id) references project(id)
);

create table if not exists worker(
id int not null auto_increment primary key,
full_name varchar(50) not null,
title varchar(50) not null,
salary double not null,
team_lead_id int default null,
foreign key(team_lead_id) references team_lead(id)
);
package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    private int id;
    private int total;
    private Set<Cart> carts = new HashSet<>();

    public Item(int total) {
        this.total = total;
    }
}

package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    private int id;
    private int total;
    private String name;
    private Set<Item> items = new HashSet<>();

    public Cart(int total, String name) {
        this.total = total;
        this.name = name;
    }
}

import model.Cart;
import model.Item;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.SessionFactoryUtil;

import java.util.Arrays;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        Session session = SessionFactoryUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Cart cart1 = new Cart(2, "Test_cart");
        Item item1 = new Item(23);
        Item item2 = new Item(22);
        Item item3 = new Item(21);

        cart1.setItems(new HashSet<>(Arrays.asList(item1, item2, item3)));


        Cart cart2 = new Cart(3, "Test_cart2");
        Item item4 = new Item(23);
        Item item5 = new Item(22);
        Item item6 = new Item(21);

        cart2.setItems(new HashSet<>(Arrays.asList(item4, item5, item6)));
        session.persist(cart1);
        session.persist(cart2);
        transaction.commit();
        session.close();





    }
}

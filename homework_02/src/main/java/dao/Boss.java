package dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Boss {
    private int bossId;
    private String bossName;
    private String bossSurname;
    private int bossAge;
}

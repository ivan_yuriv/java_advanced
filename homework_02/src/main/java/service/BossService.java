package service;

import dao.Boss;

import java.sql.SQLException;

public interface BossService {
    void createBoss(Boss boss) throws SQLException;
}

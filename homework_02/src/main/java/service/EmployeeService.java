package service;

import dao.Employee;
import exception.DuplicateEmployeeException;
import exception.NoSuchEmployeeException;

import java.sql.SQLException;
import java.util.List;

public interface EmployeeService {
    List<Employee> getAll() throws SQLException;

    Employee getEmployeeById(int employeeId) throws SQLException, NoSuchEmployeeException;

    void createEmployee(Employee employee) throws NoSuchEmployeeException, SQLException, DuplicateEmployeeException;
}

package service.impl;

import dao.Boss;
import jdbc.MySqlConnector;
import org.apache.log4j.Logger;
import service.BossService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class BossServiceImpl implements BossService {
    private static final Logger logger = Logger.getLogger(BossServiceImpl.class);
    private static Connection connection;

    static {
        try {
            connection = MySqlConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createBoss(Boss boss) throws SQLException {
        logger.warn("Warning");
        logger.fatal("Fatal Error");
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO bank_system.boss" +
                "(boss_id, boss_name, boss_surname, boss_age) VALUES(?,?,?,?)")) {
            statement.setInt(1, boss.getBossId());
            statement.setString(2, boss.getBossName());
            statement.setString(3, boss.getBossSurname());
            statement.setInt(4, boss.getBossAge());
            statement.execute();
        }
    }
}

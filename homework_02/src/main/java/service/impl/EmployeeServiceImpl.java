package service.impl;

import dao.Boss;
import dao.Employee;
import exception.DuplicateEmployeeException;
import exception.NoSuchEmployeeException;
import jdbc.MySqlConnector;
import service.BossService;
import service.EmployeeService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {
    private static Connection connection;
    private static BossService bossService = new BossServiceImpl();

    static {
        try {
            connection = MySqlConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Employee> getAll() throws SQLException {
        List<Employee> employees = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM bank_system.employee");
        while (result.next()) {
            employees.add(new Employee(result.getInt("employee_id"), result.getString("employee_name"),
                    result.getString("employee_surname"), result.getInt("employee_age")));
        }
        return employees;
    }

    @Override
    public Employee getEmployeeById(int employeeId) throws SQLException, NoSuchEmployeeException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM bank_system.employee WHERE employee_id= ?");
        statement.setInt(1, employeeId);
        ResultSet result = statement.executeQuery();
        if (result.next()) {
            return new Employee(result.getInt("employee_id"), result.getString("employee_name"),
                    result.getString("employee_surname"), result.getInt("employee_age"));
        } else throw new NoSuchEmployeeException("No user with id" + employeeId);

    }

    @Override
    public void createEmployee(Employee employee) throws NoSuchEmployeeException, SQLException, DuplicateEmployeeException {
        if (isExists(employee.getEmployeeId())) {
            throw new DuplicateEmployeeException("Employee with id : " + employee.getEmployeeId() + " already exists!");
        } else {
            System.out.println("Creating new Boss with id " + employee.getEmployeeId());
            bossService.createBoss(new Boss(employee.getEmployeeId(), employee.getEmployeeName(), employee.getEmployeeSurname(), employee.getEmployeeAge()));
            System.out.println("Creating employee with id " + employee.getEmployeeId());
            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO bank_system.employee" +
                    "(employee_id, employee_name, employee_surname, employee_age) VALUES(?,?,?,?)")) {
                statement.setInt(1, employee.getEmployeeId());
                statement.setString(2, employee.getEmployeeName());
                statement.setString(3, employee.getEmployeeSurname());
                statement.setInt(4, employee.getEmployeeAge());
                statement.execute();
            }
        }

    }

    private boolean isExists(int employeeId) throws SQLException {
        boolean flag = false;
        for (Employee employee : getAll()) {
            flag = employee.getEmployeeId() == employeeId;
        }
        return flag;
    }
}

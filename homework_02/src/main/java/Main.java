import dao.Boss;
import dao.Employee;
import exception.DuplicateEmployeeException;
import exception.NoSuchEmployeeException;
import service.BossService;
import service.EmployeeService;
import service.impl.BossServiceImpl;
import service.impl.EmployeeServiceImpl;

import java.sql.SQLException;

public class Main {
    private static EmployeeService employeeService = new EmployeeServiceImpl();
    private static BossService bossService = new BossServiceImpl();

    public static void main(String[] args) throws SQLException, NoSuchEmployeeException, DuplicateEmployeeException {
        System.out.println("---------Get All--------");
        employeeService.getAll().forEach(System.out::println);
        System.out.println("---------Get Employee by Id---------");
        System.out.println(employeeService.getEmployeeById(1));
        /*System.out.println("--------Create Boss--------");
        bossService.createBoss(new Boss(5, "Orest", "Drdrdr", 26));
        System.out.println("--------Create Employee with Boss----------");
        employeeService.createEmployee(new Employee(6, "Oksana", "Uryeyr", 19));*/





    }
}

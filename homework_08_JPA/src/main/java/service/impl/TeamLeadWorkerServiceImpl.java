package service.impl;

import model.Project;
import model.TeamLeadWorker;
import service.TeamLeadWorkerService;
import utils.FactoryManager;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class TeamLeadWorkerServiceImpl implements TeamLeadWorkerService {
    private EntityManager entityManager = FactoryManager.getEntityManager();

    @SuppressWarnings("unchecked")
    @Override
    public List<TeamLeadWorker> readAll() {
        Query query = entityManager.createQuery("SELECT tw FROM TeamLeadWorker tw");
        return (List<TeamLeadWorker>) query.getResultList();
    }

    @Override
    public TeamLeadWorker read(int id) {
        return entityManager.find(TeamLeadWorker.class, id);
    }

    @Override
    public void create(TeamLeadWorker teamLeadWorker) {
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(teamLeadWorker);
        entityManager.getTransaction().commit();

    }

    @Override
    public void delete(int id) {
        TeamLeadWorker teamLeadWorker = read(id);
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.remove(teamLeadWorker);
        entityManager.getTransaction().commit();

    }
}

package service.impl;

import model.Project;
import service.ProjectService;
import utils.FactoryManager;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Queue;

public class ProjectServiceImpl implements ProjectService {

    private EntityManager entityManager = FactoryManager.getEntityManager();

    @Override
    public Project getProjectByEmail(String email) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteria = builder.createQuery(Project.class);
        Root<Project> from = criteria.from(Project.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("email"), email));
        TypedQuery<Project> typedQuery = entityManager.createQuery(criteria);
        return typedQuery.getSingleResult();
    }

    @SuppressWarnings("uncheked")
    @Override
    public List<Project> readAll() {
        Query query = entityManager.createQuery("SELECT p FROM Project p");
        return (List<Project>) query.getResultList();
    }

    @Override
    public Project read(int id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public void create(Project project) {
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(project);
        entityManager.getTransaction().commit();

    }

    @Override
    public void delete(int id) {
        Project project = read(id);
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.remove(project);
        entityManager.getTransaction().commit();

    }
}

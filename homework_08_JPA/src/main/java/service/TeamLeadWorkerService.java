package service;

import model.TeamLeadWorker;
import utils.AbstractCrudOperations;

public interface TeamLeadWorkerService extends AbstractCrudOperations<TeamLeadWorker> {
}

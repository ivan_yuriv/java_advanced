package service;

import model.Project;
import utils.AbstractCrudOperations;

import java.util.List;

public interface ProjectService extends AbstractCrudOperations<Project> {
    Project getProjectByEmail(String email);
}

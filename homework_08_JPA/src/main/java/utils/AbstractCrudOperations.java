package utils;

import java.sql.SQLException;
import java.util.List;

public interface AbstractCrudOperations<T> {
    List<T> readAll();

    T read(int id);

    void create(T t);

    void delete(int id);

}

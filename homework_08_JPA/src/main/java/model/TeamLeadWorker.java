package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "teamlead_worker")
public class TeamLeadWorker implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "team_lead_id", referencedColumnName = "id")
    private TeamLead teamlead;

    @Id
    @ManyToOne
    @JoinColumn(name = "worker_id", referencedColumnName = "id")
    private Worker worker;

}

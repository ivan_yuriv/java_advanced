package service;

import exception.AlreadyExistException;
import exception.NotFoundException;
import jdk.nashorn.internal.ir.annotations.Ignore;
import model.TeamLead;
import org.junit.jupiter.api.*;
import service.impl.TeamLeadServiceImpl;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.List;

public class TeamLeadServiceImplTest {
    private static TeamLeadServiceImpl teamLeadService;


    @BeforeAll
    static void init() {
        teamLeadService = new TeamLeadServiceImpl();
    }

    @Test
    @DisplayName("Read all Team Leads")
    public void readAllTest() throws SQLException {
        List<TeamLead> teamLeadList = teamLeadService.readAll();
        Assertions.assertTrue(teamLeadList.size() > 0);
    }

    @Test
    @DisplayName("Read Team Lead by id")
    public void readTest() throws NotFoundException, SQLException {
        int testId = 0;
        TeamLead expected = new TeamLead(testId, "test_name", Timestamp.valueOf("1973-01-01 02:00:01"), 100);
        TeamLead actual = teamLeadService.read(testId);
        Assertions.assertEquals(expected, actual);
    }


    @Disabled
    @Test
    @DisplayName("Create and delete new Team Lead")
    public void createTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 11111;
        TeamLead teamLeadToInsert = new TeamLead(id, "Test_name", new Timestamp(System.currentTimeMillis()), 100);
        teamLeadService.create(teamLeadToInsert);
        TeamLead actual = teamLeadService.read(id);
        Assertions.assertEquals(teamLeadToInsert, actual);
        teamLeadService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> teamLeadService.read(id));
    }
}

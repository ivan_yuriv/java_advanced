drop database app;
create database if not exists app;
use app;

create table if not exists project(
id int not null primary key,
email VARCHAR(50) NOT NULL,
password VARCHAR(50),
name varchar(50) not null,
budget int not null
);

create table if not exists team_lead(
id int not null primary key,
full_name varchar(50) not null,
date_of_birth timestamp,
salary double not null,
foreign key(id) references project(id)
);

create table if not exists worker(
id int not null primary key,
full_name varchar(50) not null,
title varchar(50) not null,
salary double not null
);

create table if not exists teamlead_worker(
team_lead_id int not null,
worker_id int not null,
constraint id PRIMARY KEY(team_lead_id, worker_id),
FOREIGN KEY(team_lead_id) REFERENCES team_lead(id),
FOREIGN KEY(worker_id) REFERENCES worker(id)


);
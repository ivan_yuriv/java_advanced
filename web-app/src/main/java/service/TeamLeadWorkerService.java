package service;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.TeamLeadWorker;

import java.sql.SQLException;
import java.util.List;

public interface TeamLeadWorkerService {
    List<TeamLeadWorker> readAll() throws SQLException;

    void create(TeamLeadWorker teamLeadWorker) throws SQLException, AlreadyExistException;

    void delete(int teamLeadId, int workerId) throws SQLException, NotFoundException;
}

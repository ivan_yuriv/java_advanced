package service.impl;

import dao.TeamLeadDao;
import dao.WorkerModelDao;
import dao.impl.WorkerModelDaoImpl;
import org.apache.log4j.Logger;
import service.WorkerModelService;
import util.model.WorkerModel;

import java.sql.SQLException;
import java.util.List;

public class WorkerModelServiceImpl implements WorkerModelService {
    private WorkerModelDao workerModelDao;
    private static final Logger logger = Logger.getLogger(WorkerModelServiceImpl.class);

    public WorkerModelServiceImpl() {
        workerModelDao = new WorkerModelDaoImpl();
    }

    @Override
    public List<WorkerModel> getWorkerByTeamLead(int teamLeadId) throws SQLException {
        logger.info("Read all workers for Team Lead request");
        return workerModelDao.getWorkerByTeamLead(teamLeadId);
    }
}

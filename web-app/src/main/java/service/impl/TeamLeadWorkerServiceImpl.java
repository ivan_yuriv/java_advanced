package service.impl;

import dao.TeamLeadWorkerDao;

import dao.impl.TeamLeadWorkerDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.TeamLead;
import model.TeamLeadWorker;
import org.apache.log4j.Logger;
import service.TeamLeadWorkerService;

import java.sql.SQLException;
import java.util.List;

public class TeamLeadWorkerServiceImpl implements TeamLeadWorkerService {
    private TeamLeadWorkerDao teamLeadWorkerDao;
    private static final Logger logger = Logger.getLogger(TeamLeadWorkerServiceImpl.class);

    public TeamLeadWorkerServiceImpl() {
        teamLeadWorkerDao = new TeamLeadWorkerDaoImpl();
    }

    @Override
    public List<TeamLeadWorker> readAll() throws SQLException {
        logger.info("Read all Team Lead-Worker (s) request");
        return teamLeadWorkerDao.readAll();
    }

    @Override
    public void create(TeamLeadWorker teamLeadWorker) throws SQLException, AlreadyExistException {
        if (isExists(teamLeadWorker.getTeamLeadId(), teamLeadWorker.getWorkerId())) {
            logger.error("TeamLeadWorker with teamLead_id : " + teamLeadWorker.getTeamLeadId() + "and worker_id : "
                    + teamLeadWorker.getWorkerId() + " already exists and can`t be created");
            throw new AlreadyExistException("TeamLeadWorker with teamLead_id : " + teamLeadWorker.getTeamLeadId() + "and worker_id : "
                    + teamLeadWorker.getWorkerId() + " already exists");
        } else {
            logger.info("Creating TeamLeadWorker : " + teamLeadWorker);
            teamLeadWorkerDao.create(teamLeadWorker);
        }

    }

    @Override
    public void delete(int teamLeadId, int workerId) throws SQLException, NotFoundException {
        if (!isExists(teamLeadId, workerId)) {
            logger.info("TeamLeadWorker with teamLead_id : " + teamLeadId + "and worker_id : "
                    + workerId + " not found and can`t be deleted");
            throw new NotFoundException("TeamLeadWorker with teamLead_id : " + teamLeadId + "and worker_id : "
                    + workerId + " not found");
        } else {
            teamLeadWorkerDao.delete(teamLeadId, workerId);
            logger.error("TeamLeadWorker with teamLead_id : " + teamLeadId + "and worker_id : "
                    + workerId + " was deleted deleted");

        }

    }

    private List<TeamLeadWorker> getAll() throws SQLException {
        return teamLeadWorkerDao.readAll();
    }

    private boolean isExists(int teamLeadId, int workerId) throws SQLException {
        boolean flag = false;
        for (TeamLeadWorker entity : getAll()) {
            if (teamLeadId == entity.getTeamLeadId() && workerId == entity.getWorkerId()) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}

package service.impl;

import dao.impl.TeamLeadDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.TeamLead;
import org.apache.log4j.Logger;
import service.TeamLeadService;

import java.sql.SQLException;
import java.util.List;

public class TeamLeadServiceImpl implements TeamLeadService {
    private TeamLeadDaoImpl teamLeadDao;
    private static final Logger logger = Logger.getLogger(TeamLeadServiceImpl.class);

    public TeamLeadServiceImpl() {
        teamLeadDao = new TeamLeadDaoImpl();
    }

    @Override
    public List<TeamLead> readAll() throws SQLException {
        logger.info("Read all Team Leads request");
        return teamLeadDao.readAll();
    }

    @Override
    public TeamLead read(int id) throws SQLException, NotFoundException {
        TeamLead teamLead = teamLeadDao.read(id);
        if (teamLead == null) {
            logger.error("Team Lead with id : " + id + " not found");
            //throw new NotFoundException("Team Lead with id : " + id + " not found");
            return null;
        } else {
            logger.info("Getting Team LEad with id : " + id);
            logger.info(teamLead);
            return teamLead;
        }
    }

    @Override
    public void create(TeamLead teamLead) throws SQLException, AlreadyExistException {
        if (isExists(teamLead.getId())) {
            logger.error("Team Lead with id : " + teamLead.getId() + " already exists and can`t be created");
            throw new AlreadyExistException("Team Lead with id : " + teamLead.getId() + " already exists");
        } else {
            logger.info("Creating Team Lead : " + teamLead);
            teamLeadDao.create(teamLead);
        }

    }

    @Override
    public void delete(int id) throws SQLException, NotFoundException {
        if (isExists(id)) {
            teamLeadDao.delete(id);
            logger.info("Team Lead with id : " + id + " was deleted");
        } else {
            logger.error("Team Lead with id : " + id + " not found and can`t be deleted");
            throw new NotFoundException("Team Lead with id : " + id + " not found");
        }

    }

    private List<TeamLead> getAll() throws SQLException {
        return teamLeadDao.readAll();
    }

    private boolean isExists(int teamLeadId) throws SQLException {
        boolean flag = false;
        for (TeamLead teamLead : getAll()) {
            if (teamLead.getId() == teamLeadId) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}

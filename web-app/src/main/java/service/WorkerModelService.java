package service;

import util.model.WorkerModel;

import java.sql.SQLException;
import java.util.List;

public interface WorkerModelService {

    List<WorkerModel> getWorkerByTeamLead(int teamLeadId) throws SQLException;
}

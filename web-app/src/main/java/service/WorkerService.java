package service;

import dao.WorkerDao;
import model.Worker;
import shared.AbstractCrudOperations;

public interface WorkerService extends AbstractCrudOperations<Worker>, WorkerDao {
}

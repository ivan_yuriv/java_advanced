package dao.impl;

import dao.TeamLeadWorkerDao;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Project;
import model.TeamLeadWorker;
import util.MySqlConnector;
import util.SQLConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamLeadWorkerDaoImpl implements TeamLeadWorkerDao {
    private Connection connection;

    public TeamLeadWorkerDaoImpl() {
        connection = MySqlConnector.getConnection();
    }

    @Override
    public List<TeamLeadWorker> readAll() throws SQLException {
        List<TeamLeadWorker> teamLeadWorkers = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQLConstants.GET_ALL_TEAM_LEAD_WORKER)) {
            while (resultSet.next()) {
                TeamLeadWorker teamLeadWorker = new TeamLeadWorker(resultSet.getInt("team_lead_id"),
                        resultSet.getInt("worker_id"));

                teamLeadWorkers.add(teamLeadWorker);
            }
        }
        return teamLeadWorkers;
    }

    @Override
    public void create(TeamLeadWorker teamLeadWorker) throws SQLException, AlreadyExistException {
        executeQuery(SQLConstants.INSERT_TEAM_LEAD_WORKER, teamLeadWorker);

    }

    @Override
    public void delete(int teamLeadId, int workerId) throws SQLException, NotFoundException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.DELETE_TEAM_LEAD_WORKER)) {
            statement.setInt(1, teamLeadId);
            statement.setInt(2, workerId);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void executeQuery(String query, TeamLeadWorker teamLeadWorker) {
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, teamLeadWorker.getTeamLeadId());
            statement.setInt(2, teamLeadWorker.getWorkerId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}

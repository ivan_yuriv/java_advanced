package dao.impl;

import com.mysql.cj.x.protobuf.MysqlxPrepare;
import dao.WorkerModelDao;
import model.Worker;
import util.MySqlConnector;
import util.SQLConstants;
import util.model.WorkerModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkerModelDaoImpl implements WorkerModelDao {
    private Connection connection;

    public WorkerModelDaoImpl() {
        connection = MySqlConnector.getConnection();
    }

    @Override
    public List<WorkerModel> getWorkerByTeamLead(int teamLeadId) throws SQLException {
        List<WorkerModel> workers = new ArrayList<>();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.GET_WORKERS_FOR_TEAM_LEAD)) {
            statement.setInt(1, teamLeadId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                WorkerModel worker = new WorkerModel(resultSet.getInt("id"), resultSet.getString("full_name"),
                        resultSet.getString("title"), resultSet.getDouble("salary"));
                workers.add(worker);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            resultSet.close();
        }
        return workers;
    }
}

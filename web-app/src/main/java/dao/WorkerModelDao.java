package dao;

import util.model.WorkerModel;

import java.sql.SQLException;
import java.util.List;

public interface WorkerModelDao {
    List<WorkerModel> getWorkerByTeamLead(int teamLeadId) throws SQLException;
}

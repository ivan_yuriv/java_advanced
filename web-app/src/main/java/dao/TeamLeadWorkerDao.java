package dao;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.TeamLeadWorker;
import shared.AbstractCrudOperations;

import java.sql.SQLException;
import java.util.List;

public interface TeamLeadWorkerDao {
    List<TeamLeadWorker> readAll() throws SQLException;

    void create(TeamLeadWorker teamLeadWorker) throws SQLException, AlreadyExistException;

    void delete(int teamLeadId, int workerId) throws SQLException, NotFoundException;
}

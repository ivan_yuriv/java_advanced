package util;

public class SQLConstants {
    public static final String GET_ALL_PROJECTS = "SELECT * FROM app.project";
    public static final String GET_PROJECT_BY_ID = "SELECT * FROM app.project WHERE id = ?";
    public static final String GET_PROJECT_BY_EMAIL = "SELECT * FROM app.project WHERE email = ?";
    public static final String INSERT_PROJECT = "INSERT INTO app.project (id, email, password, name, budget) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE_PROJECT = "UPDATE app.project SET id = ?, email = ?, password = ?, name = ?, budget = ? WHERE (id = ?)";
    public static final String DELETE_PROJECT_BY_ID = "DELETE FROM app.project WHERE id = ?";

    public static final String GET_ALL_WORKERS = "SELECT * FROM app.worker";
    public static final String GET_WORKER_BY_ID = "SELECT * FROM app.worker WHERE id = ?";
    public static final String INSERT_WORKER = "INSERT INTO app.worker (id, full_name, title, salary) VALUES (?, ?, ?, ?)";
    public static final String UPDATE_WORKER = "UPDATE app.worker SET full_name = ?, title = ?, salary = ? WHERE (id = ?)";
    public static final String DELETE_WORKER_BY_ID = "DELETE FROM app.worker WHERE id = ?";

    public static final String GET_ALL_TEAM_LEADS = "SELECT * FROM app.team_lead";
    public static final String GET_TEAM_LEAD_BY_ID = "SELECT * FROM app.team_lead WHERE id = ?";
    public static final String INSERT_TEAM_LEAD = "INSERT INTO app.team_lead (id, full_name, date_of_birth, salary) VALUES (?, ?, ?, ?)";
    public static final String DELETE_TEAM_LEAD_BY_ID = "DELETE FROM app.team_lead WHERE id = ?";

    public static final String INSERT_TEAM_LEAD_WORKER = "INSERT INTO app.teamlead_worker (team_lead_id, worker_id) VALUES (?, ?)";
    public static final String DELETE_TEAM_LEAD_WORKER = "DELETE FROM app.teamlead_worker WHERE team_lead_id = ? AND worker_id = ?";
    public static final String GET_ALL_TEAM_LEAD_WORKER = "SELECT * FROM app.teamlead_worker";
    public static final String GET_WORKERS_FOR_TEAM_LEAD = "SELECT app.worker.id, app.worker.full_name, app.worker.title, app.worker.salary, " +
            "FROM app.worker INNER JOIN app.teamlead_worker ON app.worker.id = app.teamlead_worker.team_lead_id " +
            "WHERE app.teamlead_worker.worker_id = ?";


}

package servlet.controller;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import model.Project;
import model.TeamLead;
import model.TeamLeadWorker;
import org.apache.log4j.Logger;
import service.ProjectService;
import service.TeamLeadService;
import service.TeamLeadWorkerService;
import service.WorkerModelService;
import service.impl.ProjectServiceImpl;
import service.impl.TeamLeadServiceImpl;
import service.impl.TeamLeadWorkerServiceImpl;
import service.impl.WorkerModelServiceImpl;
import util.model.WorkerModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@WebServlet("/teamLead")
public class teamLeadController extends HttpServlet {
    private static final Logger logger = Logger.getLogger(teamLeadController.class);
    private TeamLeadWorkerService teamLeadWorkerService;
    private TeamLeadService teamLeadService;
    private WorkerModelService workerModelService;
    private ProjectService projectService;

    public teamLeadController() throws SQLException, ClassNotFoundException {
        teamLeadWorkerService = new TeamLeadWorkerServiceImpl();
        teamLeadService = new TeamLeadServiceImpl();
        workerModelService = new WorkerModelServiceImpl();
        projectService = new ProjectServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("GET request : get workers by teamLead");
        int teamLeadId = getProjectIdAndTeamLeadIdParameter(req);
        List<WorkerModel> workers = workerModelService.getWorkerByTeamLead(teamLeadId);
        logger.info("Workers : " + workers);
        String json = new Gson().toJson(workers);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(json);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("POST request : add worker to teamLead");
        int workerId = Integer.parseInt(req.getParameter("workerId"));
        int teamLeadId = getProjectIdAndTeamLeadIdParameter(req);
        if (Objects.isNull(teamLeadService.read(teamLeadId))) {
            TeamLead teamLead = new TeamLead(teamLeadId);
            logger.info("POST request : new Team Lead was added : " + teamLead);
            teamLeadService.create(teamLead);
            TeamLeadWorker teamLeadWorker = new TeamLeadWorker(teamLeadId, workerId);
            logger.info("Worker : " + workerId + ", was added to TeamLead: " + teamLeadId);
            teamLeadWorkerService.create(teamLeadWorker);
            resp.setContentType("text");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write("Success");

        }
    }

    @SneakyThrows
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("DELETE request : delete worker from Team Lead");

        int workerId = Integer.parseInt(req.getParameter("workerId"));
        int teamLeadId = getProjectIdAndTeamLeadIdParameter(req);

        teamLeadWorkerService.delete(teamLeadId, workerId);
        logger.info("Deleted worker : " + workerId + " , from Team Lead : " + teamLeadId);

        resp.setContentType("text");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");
    }

    private int getProjectIdAndTeamLeadIdParameter(HttpServletRequest req) throws SQLException {
        HttpSession session = req.getSession();
        String projectEmail = session.getAttribute("projectEmail").toString();
        Project project = projectService.readByEmail(projectEmail);
        return project.getId();
    }
}

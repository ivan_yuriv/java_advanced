package servlet.controller;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import model.Worker;
import org.apache.log4j.Logger;
import service.WorkerService;
import service.impl.WorkerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/workers")
public class WorkersGetController extends HttpServlet {
    private WorkerService workerService;
    private static final Logger logger = Logger.getLogger(WorkersGetController.class);

    public WorkersGetController() {
        workerService = new WorkerServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Worker> workers = workerService.readAll();
        String json = new Gson().toJson(workers);
        logger.info("GET /workers request ");
        logger.info("Workers : " + json);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(json);
    }
}

$("button#add").click(function () {
    let workerId = jQuery(this).attr("worker-id");

    $.post("teamLead", {'workerId': workerId}, function (data) {
        if (data == 'Success') {
            alert("Worker with id : " + workerId + " was added to Team Lead");
        }
    });
});


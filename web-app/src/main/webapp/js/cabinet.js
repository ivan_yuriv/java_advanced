let workers = null;

$.get("workers", function (data) {
    if (data != '') {
        workers = data;
    }

}).done(function () {
    let cardsContent = "";
    jQuery.each(workers, function (i, worker) {
        cardsContent += "<div class=\'card\'>\n" +
            "<div class='card-body'>" +
            "<h5 class='card-title'>Full Name : " + worker.fullName + "</h5>" +
            "<h6 class='card-subtitle mb-2 text-muted'>Title : " + worker.title + "</h6>" +
            "<p class='card-text'>Salary : " + worker.salary + "</p>" +
            "<a href='worker?id=" + worker.id + "'class='card-link'>About worker</a>" +
            "</div>\n" +
            "</div>"

    });
    $("div#workerCards").html(cardsContent);

});